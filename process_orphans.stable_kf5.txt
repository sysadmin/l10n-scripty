# Rule file for the process_orphans.sh script
# Comments must start with the # character at first column with a space after it
# Empty lines are allowed
# Format: command origin [destination] [revision] [date]
# Available commands: move copy delete merge mergekeep
# Use only *one* space to separate data
# Note: origin and destination must be PO files, not POT files

delete messages/kdevelop/kdevkdeprovider.po

delete messages/kig/kfile_drgeo.po
delete messages/kig/kfile_kig.po

delete docmessages/kde-dev-scripts/scripts_man-reportview.1.po
delete docmessages/kde-dev-scripts/scripts_man-transxx.1.po

move messages/kpat/kpat_mimetypes.po messages/kpat/kpat_xml_mimetypes.po

delete messages/kalgebra/kalgebra._json_.po
delete messages/ksysguard/ksysguard._json_.po

# not handled by orphans: plasma-phone-components directory renamed as plasma-mobile
move messages/plasma-mobile/plasma-phone-components._desktop_.po messages/plasma-mobile/plasma-mobile._desktop_.po

copy messages/plasma-desktop/kcm_search.po messages/plasma-desktop/kcm_krunnersettings.po

move messages/kwin/kcm-kwin-scripts.po messages/kwin/kcm_kwin_scripts.po

move messages/plasma-desktop/kcm_recentFiles5.po messages/plasma-desktop/kcm_recentFiles.po

delete docmessages/libksieve/kioslave5_sieve.po

copy messages/kscreen/kscreen.po messages/kscreen/kscreen_common.po

move docmessages/plasma-desktop/kcontrol_cursortheme.po docmessages/plasma-workspace/kcontrol_cursortheme.po
delete docmessages/plasma-desktop/kcontrol_kcmlaunchfeedback.po

# archived and/or branch disabled
delete docmessages/ksysguard/ksysguard.po
delete messages/ksysguard/ksysguard._desktop_.po
delete messages/ksysguard/ksysguard._json_.po
delete messages/ksysguard/ksysguard.po
delete messages/ksysguard/org.kde.ksysguard.appdata.po

move messages/kgamma/kgamma5._desktop_.po messages/kgamma/kgamma._desktop_.po
